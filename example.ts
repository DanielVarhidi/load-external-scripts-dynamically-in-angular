import { Component, OnInit } from '@angular/core';

const url = '../../../content/js/ckeditor/ckeditor.js';

@Component({

})
export class ClassName implements OnInit {

  constructor() { }

  ngOnInit() {
  this.loadScript();
  }
  
  private loadScript() {
        console.log('preparing to load...')
        let node = document.createElement('script');
        node.src = url;
        node.type = 'text/javascript';
        node.async = true;
        node.charset = 'utf-8';
        document.getElementsByTagName('head')[0].appendChild(node);
    }

}